// server.js
// where your node app starts

// init project
var express = require('express');
var app = express();

// enable CORS (https://en.wikipedia.org/wiki/Cross-origin_resource_sharing)
// so that your API is remotely testable by FCC
var cors = require('cors');
app.use(cors({ optionsSuccessStatus: 200 }));  // some legacy browsers choke on 204

// http://expressjs.com/en/starter/static-files.html
app.use(express.static('public'));

// http://expressjs.com/en/starter/basic-routing.html
app.get("/", function (req, res) {
  res.sendFile(__dirname + '/views/index.html');
});


// your first API endpoint...
app.get("/api/hello", function (req, res) {
  res.json({ greeting: 'hello API' });
});
app.get('/', (req, res) => {
  res.sendFile(__dirname + "/views/index.html");
});

app.get('/api/timestamp', (req, res) => {
  res.json({
    unix: new Date().getTime(),
    utc: new Date().toUTCString()
  });
});

app.get('/api/timestamp/:date_string', (req, res) => {
  const date_string = req.params.date_string;
  // Check wheather the date_string consist of digits only.
  if (/^\d*$/.test(date_string)) {
    // Create a new Date object by casting date_string to Number from String (default parameter type).
    const date = new Date(Number(date_string));
    res.json({
      unix: date.getTime(),
      utc: date.toUTCString()
    });
  }
  else {
    const date = new Date(date_string);
    // Check if the date is a valid date object.
    if (date.toString() === "Invalid Date") {
      res.json({error: "Invalid Date"});
    }
    else {
      res.json({
        unix: date.getTime(),
        utc: date.toUTCString()
      });
    }
  }
});

app.listen(3000, () => {
  console.log("Server is running on loclhost:3000");
});